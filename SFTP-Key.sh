#!/bin/bash

# 引数から取得 $1 = user-name, $2 = IP Address
UserName="$1"
BaseDir=""
Group=""

# 既存ユーザと比較
CheckUser=`grep "$UserName" /etc/passwd | cut -d: -f1 | cat`

# 引数未入力
if [ "$1" = "" ] || [ "$2" = "" ]; then
    echo "引数を入力してください(案件名, IPアドレス)"
    exit 0
fi

if [ "$UserName" = "$CheckUser" ]; then
    # ユーザがいる場合処理終了
    echo "既にユーザが存在しています"
    exit 0
else
    # ユーザがいない場合作成
    echo "ユーザがないので新規作成します"
    useradd --base-dir "$BaseDir" $UserName -g "$Group"

    # Keyを作成&設置
    mkdir -p "$BaseDir"/"$UserName"/.ssh/
    ssh-keygen -t rsa -b 4096 -C "$UserName" -N "" -f "$BaseDir"/"$UserName"/.ssh/"$UserName"
    chmod 700 "$BaseDir"/"$UserName"/.ssh
    chmod 400 "$BaseDir"/"$UserName"/.ssh/*
    chown -R "$UserName":"$UserName" "$BaseDir"/"$UserName"/.ssh
    mv "$BaseDir"/"$UserName"/.ssh/"$UserName".pub "$BaseDir"/"$UserName"/.ssh/authorized_keys

    # IP制限の設定(/etc/ssh.d/sshd)
    echo "" >> /etc/ssh/sshd_config
    echo "# "$UserName" IP Address setting" >> /etc/ssh/sshd_config
    echo "Match User "$UserName"" >> /etc/ssh/sshd_config
    echo "ChrootDirectory "$BaseDir"/%u" >> /etc/ssh/sshd_config
    echo "ForceCommand internal-sftp" >> /etc/ssh/sshd_config
    echo "X11Forwarding no" >> /etc/ssh/sshd_config
    echo "AllowTcpForwarding no" >> /etc/ssh/sshd_config
    echo "AllowUsers "$UserName"@"$2"" >> /etc/ssh/sshd_config
    echo "PasswordAuthentication no" >> /etc/ssh/sshd_config

    # sshd再起動
    systemctl restart sshd

    echo "作成完了しました"
fi